#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <cmath>
#include <iostream>


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent),
    ui(new Ui::MainWindow),
    variableEdgePopUp(new variableEdgeSetup)
{
    ui->setupUi(this);

    connect(ui->popUpButton, SIGNAL(clicked()), variableEdgePopUp, SLOT(show()));
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::plotStuff()
{
    QVector<double> times;

    double Tmax = 1000;
    double step = 10;
    long N = lround(Tmax/step);
    std::cout << "Number of steps: " << N << std::endl;
    for (int s=0; s<N+1; ++s)
    {
        times.push_back(step*s);
    }

    //method with 2 data points per edge
    QVector<double> timesA {times};
    //double Td = timesA[4];
    //timesA.insert(5,Td);
    //Td = timesA[9];
    //timesA.insert(10,Td);

    //method with steps LineStyle
    times[4] = step*(4.-.5);
    times.insert(5, 4.5*step);
    times[8] = step*(8.-.5);
    times.insert(9, 8.5*step);
//    for (double& t: times)
//    {
//        std::cout << t << " ";
//    }
//    std::cout << std::endl;

    QVector<double> binaryLvlsA(timesA.size(), 0.);
    binaryLvlsA[5] = 1.;
    binaryLvlsA[6] = 1.;
    binaryLvlsA[7] = 1.;
    binaryLvlsA[8] = 1.;
    binaryLvlsA[9] = 1.;

    QVector<double> binaryLvls (times.size(), 0.);
    binaryLvls[5] = 1.;
    binaryLvls[6] = 1.;
    binaryLvls[7] = 1.;
    binaryLvls[8] = 1.;
//    for (double& l: binaryLvls)
//    {
//        std::cout << l << " ";
//    }
//    std::cout << std::endl;

    QVector<double> x,y1,y2,y3;
    for (int i=0; i<101; ++i)
    {
        x.push_back(2.*3.14159*i/100.);
        y1.push_back(sin(x[i]));
        y2.push_back(sin(2.*x[i]));
        y3.push_back(sin(0.5*x[i]));
    }

    ui->plot_1->addGraph();
    ui->plot_1->graph()->setLineStyle(QCPGraph::LineStyle::lsStepCenter);
    ui->plot_2->addGraph();

    //PLOT 1
    ui->plot_1->graph(0)->setData(times,binaryLvls);
    ui->plot_1->yAxis->setRange(-.1,1.1);
    ui->plot_1->xAxis->setRange(0, Tmax);

    //PLOT 2
    ui->plot_2->graph(0)->setData(timesA, binaryLvlsA, true);
    QVector<double> vKey = {40., 90.};
    QVector<double> vVal = {1., 0.};
    ui->plot_2->graph(0)->addData(vKey, vVal, true);
    double W = 0.5;
    std::for_each(ui->plot_2->graph(0)->data()->constBegin(), ui->plot_2->graph(0)->data()->constEnd(), [](QCPGraphData data){std::cout << data.key << " ";});
    std::cout << std::endl;

    //---------- testing iterator dereferencing
    std::vector<edgeProp> Vedges;
    edgeProp e1 = {11., nullptr};
    edgeProp e2 = {20., nullptr};
    edgeProp e3 = {60. , nullptr};
    edgeProp e4 = {35., nullptr};
    Vedges.push_back(e1);
    Vedges.push_back(e2);
    Vedges.push_back(e3);
    Vedges.push_back(e4);
    double Tedge = 60.;
    auto found = std::find_if(Vedges.begin(), Vedges.end(), [&Tedge](edgeProp edge){return fabs(edge.timestamp-Tedge)<0.001;});
    //2. find the double timestamp for the corresponding falling edge (which has the same index as the rising edge in the std::vector because they are sorted)
    std::cout << "With found-Vedges.begin(): " << Vedges[found-Vedges.begin()].timestamp << std::endl;
    std::cout << "With found->timestamp: " << found->timestamp << std::endl;
    for (edgeProp &e : Vedges)
    {
        std::cout << e.timestamp << "  ";
    }
    std::cout << std::endl;
    Vedges.erase(found);
    for (edgeProp &e : Vedges)
    {
        std::cout << e.timestamp << "  ";
    }
    std::cout << std::endl;
//    for (int j=50; j< 60; ++j)
//    {
//        QCPGraphData* ptr = ui->plot_2->graph(0)->data()->begin()+j;
//        ptr->value = W;
//    }
    ui->plot_2->yAxis->setRange(-.1,1.1);
    ui->plot_2->xAxis->setRange(0, Tmax);
    ui->plot_2->graph()->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, 5));
    ui->plot_2->setInteraction(QCP::iSelectPlottables);
    ui->plot_2->graph(0)->setSelectable(QCP::stSingleData);


//    std::vector<edgeProp> vec;
//    QCPItemLine* risingA1 = new QCPItemLine(ui->plot_2);
//    edgeProp rising1 {50., risingA1};
//    vec.push_back(rising1);
//    QCPItemLine* risingA2 = new QCPItemLine(ui->plot_2);
//    edgeProp rising2 {20.0, risingA2};
//    vec.push_back(rising2);
//    QCPItemLine* risingA3 = new QCPItemLine(ui->plot_2);
//    edgeProp rising3 {180., risingA3};
//    vec.push_back(rising3);
//    QCPItemLine* risingA4 = new QCPItemLine(ui->plot_2);
//    edgeProp rising4 {5., risingA4};
//    vec.push_back(rising4);
//    std::cout << "before sort:\n";
//    std::for_each(vec.begin(), vec.end(), [](edgeProp e){std::cout << e.timestamp << ',' << e.arrow << '\n';});
//    std::sort(vec.begin(), vec.end(), [](edgeProp e1, edgeProp e2){return (e1.timestamp < e2.timestamp);});
//    std::cout << "after sort:\n";
//    std::for_each(vec.begin(), vec.end(), [](edgeProp e){std::cout << e.timestamp << ',' << e.arrow << '\n';});

}

//void MainWindow::on_popUpButton_clicked()
//{
//    Ui::variableEdgeSetup::
//}

void MainWindow::on_parseSeq1_clicked()
{
    ui->sequence_1->seqData->parseAndPlot();
}

void MainWindow::on_parseSeq2_clicked()
{
    ui->sequence_2->seqData->parseAndPlot();
}

void MainWindow::on_parseSeq3_clicked()
{
    ui->sequence_3->seqData->parseAndPlot();
}


std::vector<int> timestampsVec_doubleToUInt(std::vector<edgeProp> seqData, double step)
{
    std::vector<int> result;
    result.resize(seqData.size());

    unsigned int u;
    for( int k = 0; k<seqData.size(); ++k)
    {
        u = lround(seqData[k].timestamp/step);
        result[k] = u;
    }

    return result;
}

//examines what edges there are in successive 64bit chunks, makes a corresponding
//64bit word per channel and parallelize them into the output buffer

//vector timestamps_rise and timestamps_fall are just indices of edges as INTEGERS, i.e. the timestamps in second will be this integer multiplying by time step
void MainWindow::translateTimestampsToParallel(int seqLength, int chanNb, std::vector< std::vector<int> > timestamps_rise, std::vector< std::vector<int> > timestamps_fall, int16_t* pBuffer)
{

    if (seqLength%64 != 0)
    {
        std::cout << "Error: Length of sequence should be a multiple of 64 !!" << std::endl;
        return;
    }

    //help-vectors to store transitions found within one 64 bit chunk, one vector of ints for each channel
    //std::vector< std::vector<int> > risingEdgesInChunk (chanNb);
    std::vector<int> risingEdgesInChunk;
    //std::vector< std::vector<int> > fallingEdgesInChunk (chanNb);
    std::vector<int> fallingEdgesInChunk;

    //vector used to store temporary 64bit sequence chunk of
    //successive logic levels for each channel before parallelizing these chunks
    std::vector<uint64_t> binChunckV;
    binChunckV.resize(chanNb);

    int start_t, stop_t, eRise, eFall, rising_nb, falling_nb, timeInd;
    int index = 0;
    int found_val = -1;
    std::vector<int>::iterator found_ind;
    uint64_t lvl, word;
    bool lvl_changed;

    auto test_timestampInChunck = [&start_t, &stop_t, &found_val](int T){return (T>=start_t)&&(T<=stop_t)&&(T>found_val);}; //predicate for finding edges; between start and stop and after the preceding found value

    for (int i=0; i<seqLength/64; ++i)//go through the entire length of the sequence
    {
        start_t = i*64;
        stop_t = start_t+63;
        std::cout << "Chunk " << i << ":" << std::endl;

        for (int ch=0; ch<chanNb; ++ch) //for each channel, with a max number of channel of 16
        {
            //reset the vectors of edges index for this chunk
            //risingEdgesInChunk[ch].resize(0);
            //fallingEdgesInChunk[ch].resize(0);
            risingEdgesInChunk.resize(0);
            fallingEdgesInChunk.resize(0);

            //look what edges are in the 64 cycle long time interval
            //1. rising edges
            do
            {
                found_ind = std::find_if(timestamps_rise[ch].begin(), timestamps_rise[ch].end(), test_timestampInChunck);
                if (found_ind != timestamps_rise[ch].end())
                {
                    found_val = *found_ind;
                    std::cout << "rising found at " << found_val << std::endl;
                    risingEdgesInChunk.push_back(found_val);
                    //risingEdgesInChunk[ch].push_back(found_val);
                }
            } while (found_ind != timestamps_rise[ch].end());

            found_val = -1;
            //2. falling edges
            do
            {
                found_ind = std::find_if(timestamps_fall[ch].begin(), timestamps_fall[ch].end(), test_timestampInChunck);
                if (found_ind != timestamps_fall[ch].end())
                {
                    found_val = *found_ind;
                    std::cout << "falling found at " << found_val << std::endl;
                    fallingEdgesInChunk.push_back(found_val);
                    //fallingEdgesInChunk[ch].push_back(found_val);
                }

            } while (found_ind != timestamps_fall[ch].end());


            auto printVec = [](int K){std::cout << K << "  ";};
            for_each(risingEdgesInChunk.begin(), risingEdgesInChunk.end(), printVec);
            for_each(fallingEdgesInChunk.begin(), fallingEdgesInChunk.end(), printVec);
            //then build up a 64bit word corresponding to these transitions
            //Least Significant Bit corresponds to time closest to 0
            eRise = 0;
            eFall = 0;
            word = 0ULL;
            lvl = 0ULL;
            //first a sanity check: cannot have a difference of more than 1 rising or 1 falling edge between
            //the amount of edges found in the chunk
            if (std::fabs((double) (risingEdgesInChunk.size() - fallingEdgesInChunk.size()) ) > 1.)
            {
                std::cout << "Error: the difference in amount of rising and falling edges is too high; impossible sequence !!" << std::endl;
                return;
            }

            rising_nb = risingEdgesInChunk.size();
            falling_nb = fallingEdgesInChunk.size();
            if (rising_nb == 0 && falling_nb == 0) //no edges where found at all
            {
                if(lvl)
                    word = 0xFFFFFFFFFFFFFFFF; //64 1s
                    //else word stays equal to 64 0s
            }
            else if (rising_nb == 1 && falling_nb == 0)//only one rising edge found; it means the level is low and changes to high
            {
                //index = risingEdgesInChunk[ch][0];
                index = risingEdgesInChunk[0];
                for (uint8_t b = index; b<64 ; ++b)
                {
                    word += 1ULL<<b;
                }
            }
            else if (rising_nb == 0 && falling_nb == 1)//only one falling edge found; it means the level is high and changes to low
            {
                //index = fallingEdgesInChunk[ch][0];
                index = fallingEdgesInChunk[0];
                for (uint8_t b = 0; b<index ; ++b)
                {
                    word += 1ULL<<b;
                }
            }
            else //at least 1 rising and 1 falling edge found
            {
                for(int b = 0; b<64; ++b)
                {
                    timeInd = 64*i + b;
                    lvl_changed = false;
                    //if(64*i + b == risingEdgesInChunk[ch][eRise])
                    if(eRise < rising_nb) //check if we haven't already seen all rising edges
                    {
                        if(timeInd == risingEdgesInChunk[eRise])//if there is a rising edge at this bit
                        {
                            lvl = 1ULL; //change level to 1
                            word += (lvl<<b);
                            ++eRise;
                            lvl_changed = true;
                        }
                    }

                    //else if (64*i + b == fallingEdgesInChunk[ch][eFall])
                    if(eFall < falling_nb) //check if we haven't already seen all falling edges
                    {
                        if (timeInd == fallingEdgesInChunk[eFall]) //if there is a falling edge at this bit
                        {
                            lvl = 0ULL; //changes level to 0
                            ++eFall;
                            lvl_changed = true;
                        }
                    }

                    if(!lvl_changed) //otherwise just keep the current level
                    {
                        word += (lvl<<b);
                    }

                    std::cout << "lvl = " << lvl << std::endl;
                }
            }
            //finally store the obtained 64bit words for this channel
            binChunckV.push_back(word);

            std::cout << "ch" << ch << ": " << integ_to_binStr(word, true) << '\n';


        }


        /*
        //now parallelize the chanNb 64bit chunk into (64bit*chanNb/16bit =) 4*chanNb 16bit words
        //fill with additional empty channel so it adds up to 8 or 16
        std::vector<size_t> possible_sizes = {2,4,8,16};
        int16_t two_bytes;
        if(std::find(possible_sizes.begin(), possible_sizes.end(), chanNb) != possible_sizes.end())//if the number of channel is valid
        {
            int m = 0;
            for (int shift = 0; shift < 64; ++shift)
            {
                two_bytes = 0;
                for(int ch = 0; ch < chanNb; ++ch)
                {
                    if((uint64_t)(1<<shift) & binChunckV[ch])
                        two_bytes += (int16_t)(1<<(m%16));
                    if(m%16==15)
                        *pBuffer++ = two_bytes;
                    ++m;
                }
            }
        }
        else
        {
            std::cout << "Number of channels not available !" << std::endl;
        }
        */
    }
}


void MainWindow::on_printEdges_clicked()
{
    std::vector< std::vector<int> > risingE_ints;
    risingE_ints.push_back(timestampsVec_doubleToUInt(ui->sequence_1->seqData->risingEdges, ui->sequence_1->step));
    risingE_ints.push_back(timestampsVec_doubleToUInt(ui->sequence_2->seqData->risingEdges, ui->sequence_2->step));
    risingE_ints.push_back(timestampsVec_doubleToUInt(ui->sequence_3->seqData->risingEdges, ui->sequence_3->step));

    std::vector< std::vector<int> > fallingE_ints;
    fallingE_ints.push_back(timestampsVec_doubleToUInt(ui->sequence_1->seqData->fallingEdges, ui->sequence_1->step));
    fallingE_ints.push_back(timestampsVec_doubleToUInt(ui->sequence_2->seqData->fallingEdges, ui->sequence_2->step));
    fallingE_ints.push_back(timestampsVec_doubleToUInt(ui->sequence_3->seqData->fallingEdges, ui->sequence_3->step));

    translateTimestampsToParallel(lround(ui->sequence_1->Tmax/ui->sequence_1->step)+1, 3, risingE_ints, fallingE_ints, nullptr);
}
