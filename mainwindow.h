#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "variableedgesetup.h"
#include "ui_variableedgesetup.h"


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    QVector<double> risingPts;
    QVector<double> fallingPts;

    void plotStuff();

    void translateTimestampsToParallel(int seqLength, int chanNb, std::vector< std::vector<int> > timestamps_rise, std::vector< std::vector<int> > timestamps_fall, int16_t* pBuffer);

private slots:

    //void on_popUpButton_clicked();

    void on_parseSeq1_clicked();

    void on_parseSeq2_clicked();

    void on_parseSeq3_clicked();

    void on_printEdges_clicked();

private:
    Ui::MainWindow* ui;
    variableEdgeSetup* variableEdgePopUp;
};
#endif // MAINWINDOW_H
