#include "qcpsequencedata.h"
#include "qseqeditor.h"
#include <utility>
#include <iostream>
#include <cstdio>
#include <cmath>


static const double epsilon = 0.001;

QCPSequenceData::QCPSequenceData(QCPAxis *keyAxis, QCPAxis *valueAxis, QSeqEditor* parentEd):QCPGraph(keyAxis, valueAxis), parentEditor(parentEd)
{

}

int keyFind_index(QCPGraphDataContainer dataCont, double searched)
{
    int res = -1;
    double z;
    for (int a = 0; a<dataCont.size(); ++a)
    {
        z = dataCont.at(a)->key;
        if(std::abs(z - searched) < epsilon)
        {
           res = a;
           break;
        }
    }
    return res;
}

int edgeFind_index(std::vector<edgeProp> edgeVec, double T)
{
    int res = -1;
    double time;
    for (int a = 0; a<edgeVec.size(); ++a)
    {
        time = edgeVec[a].timestamp;
        if(std::abs(time - T) < epsilon)
        {
           res = a;
           break;
        }
    }
    return res;
}


void QCPSequenceData::mousePressEvent(QMouseEvent *event, const QVariant& arg_details)
{
    //first get the key and value of the clicked data point !
    double T=0; double L = 0;
    QCPGraphDataContainer::const_iterator it = data()->constEnd();
    QVariant details;
    if (selectTest(event->pos(), false, &details))
    {
        QCPDataSelection dataPoints = details.value<QCPDataSelection>();
        if (dataPoints.dataPointCount() > 0)
        {
            it = data()->at(dataPoints.dataRange().begin());
            T = it->key;       //Timestamp
            L = it->value;     //binary Level
            std::cout << "T = " << T << ", L = " << L << std::endl;
        }
    }
    else
        return;

    //---------------------------------------------------------------------------------------
    //look for a timestamp with time value T in the std::vector risingEdges and fallingEdges
    //---------------------------------------------------------------------------------------

    int found_ind_rise = edgeFind_index(risingEdges, T);
    int found_ind_fall = edgeFind_index(fallingEdges, T);

    if(event->button()==Qt::LeftButton)
    {
        if (found_ind_fall >= 0) // if there is already a falling edge, do nothing and signal an error!
            std::cout << "Error: cannot add or remove rising edge here!" << std::endl;
        else if(L==0 && found_ind_rise<0)//case where there is no rising edge on the plot and no arrow and, as a result, no timestamp in QVector
        {
            //1. create a red arrow on the plot
            std::cout << "Adding arrow" << std::endl;
            QCPItemLine* risingA = new QCPItemLine(parentPlot());
            QPen arrowPen;
            arrowPen.setColor(Qt::red);
            risingA->setPen(arrowPen);
            risingA->setHead(QCPLineEnding::esSpikeArrow);
            //2. set coordinates and make sure they have the right coordinate system and are not using pixel coordinate
            risingA->start->setType(QCPItemPosition::ptPlotCoords);
            risingA->start->setCoords(T, .25);
            risingA->end->setType(QCPItemPosition::ptPlotCoords);
            risingA->end->setCoords(T, .75);
            //3. register the arrow in the vector with its timestamp
            edgeProp rising {T, risingA};
            risingEdges.push_back(rising);
            std::sort(risingEdges.begin(), risingEdges.end(), [](edgeProp e1, edgeProp e2){return (e1.timestamp < e2.timestamp);});
            //4. replot
            parentPlot()->replot();
        }
        else if (L==0 && found_ind_rise>=0 && risingEdges[found_ind_rise].arrow!=nullptr) //case where there is no rising edge on the plot but already an arrow
        {
            //1. remove the arrow on the plot and erase the edge
            parentPlot()->removeItem(risingEdges[found_ind_rise].arrow); //
            risingEdges.erase(risingEdges.begin()+found_ind_rise);
            //2. replot
            parentPlot()->replot();
        }
        else if (L==1. && found_ind_rise>=0)//case where we have to remove the rising Edge from the actual plot
        {
            //1. find where the double point with same key value is in the graph corresponding to the rising edge
            double Tedge = T;
            //-------------------------------------------------------------------------------------
            //find an edge in the plot where 2 pts have the same key in the graph (found through key value and not through double pts)
            //-------------------------------------------------------------------------------------
            auto testEdge_plot = [&Tedge](QCPGraphData data){return fabs(data.key-Tedge)<epsilon;};
            auto found_plot_rise = std::find_if(data()->begin(), data()->end(), testEdge_plot);//this is the index in the graph data with key value 'time'
                                                                                               //where we have 2 of this same key value (for an edge)
            //2. find the double timestamp for the corresponding falling edge which has the same index as the rising edge in the std::vector because they are sorted
            double Tfall = fallingEdges[found_ind_rise].timestamp;  //WARNING: here we cannot dereference found_edge_R because we are using it to access fallingEdges instead or risingEdges!!!!
            std::cout << "corresponding falling edge at " << Tfall << '\n';
            Tedge = Tfall;
            auto found_plot_fall = std::find_if(data()->begin(), data()->end(), testEdge_plot);//this is the index in the graph data with key value 'time'
                                                                                          //where we have 2 of this same key value (for an edge)
            //3. remove the rising edge in the std::vector
            risingEdges.erase(risingEdges.begin()+found_ind_rise);
            //4. set points in between rising and falling edge to 0
            if(found_plot_fall+2 - found_plot_rise > 0)
                std::for_each(found_plot_rise +2, found_plot_fall, [](QCPGraphData& data){(&data)->value = 0;});
            //5. remove the points in the plot data (one on rising and one on falling edge)
            data()->remove(T-epsilon,T+epsilon);
            addData(T,0.);
            data()->remove(Tfall-epsilon,Tfall+epsilon);
            addData(Tfall,0.);
            //6. add a falling arrow at the corresponding falling edge
            QCPItemLine* fallingA = new QCPItemLine(parentPlot());
            QPen arrowPen;
            arrowPen.setColor(Qt::blue);
            fallingA->setPen(arrowPen);
            fallingA->setHead(QCPLineEnding::esSpikeArrow);
            //7. set coordinates and make sure they have the right coordinate system and are not using pixel coordinate
            fallingA->start->setType(QCPItemPosition::ptPlotCoords);
            fallingA->start->setCoords(Tfall, .75);
            fallingA->end->setType(QCPItemPosition::ptPlotCoords);
            fallingA->end->setCoords(Tfall, .25);
            //8. register the pointer to QCPItemLine arrow in the vector with its already existing timestamp
            //fallingEdges should already be sorted
            fallingEdges[found_ind_rise].arrow = fallingA;
            //9. replot
            parentPlot()->replot();
        }

    }
    else if(event->button()==Qt::RightButton)
    {
        if (found_ind_rise >= 0) // if there is already a rising edge, do nothing and signal an error!
            std::cout << "Error: cannot add or remove falling edge here!" << std::endl;
        else if(L==0 && found_ind_fall<0)//case where there is no falling edge on the plot and no arrow and, as a result, no timestamp in QVector
        {
            //1. create a blue arrow on the plot
            QCPItemLine* fallingA = new QCPItemLine(parentPlot());
            QPen arrowPen;
            arrowPen.setColor(Qt::blue);
            fallingA->setPen(arrowPen);
            fallingA->setHead(QCPLineEnding::esSpikeArrow);
            //2. set coordinates and make sure they have the right coordinate system and are not using pixel coordinate
            fallingA->start->setType(QCPItemPosition::ptPlotCoords);
            fallingA->start->setCoords(T, .75);
            fallingA->end->setType(QCPItemPosition::ptPlotCoords);
            fallingA->end->setCoords(T, .25);
            //3. register the arrow in the vector with its timestamp
            edgeProp falling {T, fallingA};
            fallingEdges.push_back(falling);
            std::sort(fallingEdges.begin(), fallingEdges.end(), [](edgeProp e1, edgeProp e2){return (e1.timestamp < e2.timestamp);});
            //4. replot
            parentPlot()->replot();
        }
        else if (L==0 && found_ind_fall>=0 && fallingEdges[found_ind_fall].arrow!=nullptr) //case where there is no falling edge on the plot but already an arrow
        {
            //1. remove the arrow on the plot and erase the edge
            parentPlot()->removeItem(fallingEdges[found_ind_fall].arrow); //
            fallingEdges.erase(fallingEdges.begin() + found_ind_fall);
            //2. replot
            parentPlot()->replot();
        }
        else if (L==1. && found_ind_fall>=0)//case where we have to remove the falling Edge from the actual plot
        {
            //1. find where the double point with same key value is in the graph corresponding to the falling edge
            double Tedge = T;
            //-------------------------------------------------------------------------------------------------------------------------
            //find an edge in the plot where 2 pts have the same key in the graph (found through key value and not through double pts)
            //-------------------------------------------------------------------------------------------------------------------------

            auto testEdge_plot = [&Tedge](QCPGraphData data){return fabs(data.key-Tedge)<epsilon;};
            auto found_plot_fall = std::find_if(data()->begin(), data()->end(), testEdge_plot);//this is the index in the graph data with key value 'time'
                                                                                        //where we have 2 of this same key value (for an edge)
            //2. find the double timestamp for the corresponding rising edge which has the same index as the falling edge in the std::vector because they are sorted
            double Trise = risingEdges[found_ind_fall].timestamp;  //WARNING: here we cannot dereference found_edge_R because we are using it to access fallingEdges instead or risingEdges!!!!
            std::cout << "corresponding rising edge at " << Trise << std::endl;
            Tedge = Trise;
            auto found_plot_rise = std::find_if(data()->begin(), data()->end(), testEdge_plot);//this is the index in the graph data with key value 'time'
                                                                                          //where we have 2 of this same key value (for an edge)
            //3. remove the falling edge in the std::vector
            fallingEdges.erase(fallingEdges.begin() + found_ind_fall);
            //4. set points in between rising and falling edge to 0
            if(found_plot_fall+2 - found_plot_rise > 0)
                std::for_each(found_plot_rise +2, found_plot_fall, [](QCPGraphData& data){(&data)->value = 0;});
            //5. remove the points in the plot data (one on rising and one on falling edge)
            data()->remove(T-epsilon,T+epsilon);
            addData(T,0.);
            data()->remove(Trise-epsilon,Trise+epsilon);
            addData(Trise,0.);
            //6. add a rising arrow at the corresponding rising edge
            QCPItemLine* risingA = new QCPItemLine(parentPlot());
            QPen arrowPen;
            arrowPen.setColor(Qt::red);
            risingA->setPen(arrowPen);
            risingA->setHead(QCPLineEnding::esSpikeArrow);
            //7. set coordinates and make sure they have the right coordinate system and are not using pixel coordinate
            risingA->start->setType(QCPItemPosition::ptPlotCoords);
            risingA->start->setCoords(Trise, .25);
            risingA->end->setType(QCPItemPosition::ptPlotCoords);
            risingA->end->setCoords(Trise, .75);
            //8. register the pointer to QCPItemLine arrow in the vector with its already existing timestamp
            //risingEdges should already be sorted
            risingEdges[found_ind_fall].arrow = risingA;
            //9. replot
            parentPlot()->replot();

        }
    }

    std::cout <<"Rising: ";
    for(int h = 0; h<risingEdges.size(); ++h )
    {
        std::cout << "[t=" << risingEdges[h].timestamp << ", ptr=" << risingEdges[h].arrow << "] ";
    }
    std::cout << std::endl;
    std::cout <<"Falling: ";
    for(int h = 0; h<fallingEdges.size(); ++h )
    {
        std::cout << "[t=" << fallingEdges[h].timestamp << ", ptr=" << fallingEdges[h].arrow << "] ";
    }
    std::cout << std::endl;
}



void QCPSequenceData::parseAndPlot()
{
    if(risingEdges.size()!=fallingEdges.size())
    {
        std::cout << "Error: number of rising and falling edges not equal!\n" << "Add an adequate number of raising and falling edges!\n\n" << std::endl;
        return;
    }

    QCPGraphData* found_plot_rise = data()->begin();
    QCPGraphData* found_plot_fall = data()->begin()-1;
    for (int i=0; i<risingEdges.size(); ++i)
    {
        if(risingEdges[i].arrow != nullptr)//
        {
                            //std::cout << "\ni=" << i <<'\n';
            //get the i-th rising edge
            double risingEdge = risingEdges[i].timestamp;
            //get the corresponding fallingEdge
            double fallingEdge = fallingEdges[i].timestamp;
            if (fallingEdge <= risingEdge)//check that the falling edge is correct
            {
                std::cout << "Error: corresponding falling edge comes before rising edge!\n" << std::endl;
                return;
            }
            //remove the rising arrow
            parentPlot()->removeItem(risingEdges[i].arrow);
            risingEdges[i].arrow = nullptr;
            //find the index of the rising edge in the plot data starting at the last flling edge (first falling edge "is index -1")
            double Tedge = risingEdge;
            auto testEdge = [&Tedge](QCPGraphData data){return fabs(data.key-Tedge)<epsilon;};
                            //found_plot_rise = std::find_if(found_plot_fall+1, data()->end(), testEdge);
            found_plot_rise = std::find_if(data()->begin(), data()->end(), testEdge);
                            //std::cout << "found_plot_rise after: " << found_plot_rise << "\nderef: " << found_plot_rise->key << std::endl;
            //remove the falling arrow
            parentPlot()->removeItem(fallingEdges[i].arrow);
            fallingEdges[i].arrow = nullptr;
            //find the index of the falling edge in the plot data starting at the last rising edge
            Tedge = fallingEdge;
                            //found_plot_fall = std::find_if(found_plot_rise+1, data()->end(), testEdge);
            found_plot_fall = std::find_if(data()->begin(), data()->end(), testEdge);
                            //std::cout << "found_plot_fall after: " << found_plot_fall << "\nderef: " << found_plot_fall->key << std::endl;
            std::for_each(found_plot_rise+1, found_plot_fall+1, [](QCPGraphData& data){(&data)->value = 1;});
                            //std::cout << "After for_each:\n" << "found_plot_rise: " << found_plot_rise << '\n' << "found_plot_fall: " << found_plot_fall << std::endl;
            //add a point at the rising edge and at the falling edge
            QVector<double> new_keys = {risingEdge, fallingEdge};
            QVector<double> new_values = {1., 0.};
            addData(new_keys, new_values, true);
            std::cout << "After add_data:\n" << "found_plot_rise: " << found_plot_rise << '\n' << "found_plot_fall: " << found_plot_fall << std::endl;
            //replot
            parentPlot()->replot();
        }
    }
    int N = lround(parentEditor->Tmax/parentEditor->step);
    int total_nb_pts = N+1 + 2*risingEdges.size();
    std::cout << "\nNumber of plot point should be " << total_nb_pts << '\n' << "there are " << data()->size() << " data points" << std::endl;
    if(total_nb_pts!=data()->size())
        std::cout << "Warning : Something is wrong with the total amount of data points!" << std::endl;
}



void QCPSequenceData::printAllEdges()
{

}




