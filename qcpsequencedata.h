#ifndef QCPSEQUENCEDATA_H
#define QCPSEQUENCEDATA_H
#include "qcustomplot.h"
#include <unordered_map>
#include <iostream>

template <typename T>
std::string integ_to_binStr(T a, bool comma = false)
{
    if(sizeof(a)>8)
    {
        std::cout << "Error: type size larger than 8 bytes!";
        return "?";
    }
    uint64_t M;
    uint64_t aL = (uint64_t) a;
    std::string st;

    for(int i = sizeof(T)*8-1; i>=0; --i)
    {
        M=1ULL<<i;
        aL&M? st.append("1"):st.append("0");
        if((i>0)&&(i%4==0)&&comma){ st.append(",");}
    }
    return st;
}


enum class Edge
{
    noEdge,
    rising,
    falling
};

struct edgeProp
{
    double timestamp;
    QCPItemLine* arrow;
};
typedef edgeProp edgeProp;

struct varEdgeProp
{
    edgeProp maxProps;
    int stepsNb;
    double minTimestamp;
    double maxTimestamp;
};

class QSeqEditor;

class QCPSequenceData : public QCPGraph
{
public:
    QCPSequenceData(QCPAxis* keyAxis, QCPAxis* valueAxis, QSeqEditor* parentEditor);

    std::vector<edgeProp> risingEdges;
    std::vector<edgeProp> fallingEdges;

    std::list<std::string> variableT_names;
    void parseAndPlot();
    void mousePressEvent(QMouseEvent* event, const QVariant& arg_details);
    //void generateTimestampsFromVariableT(int length, int nb_of_var_T, std::unordered_map<std::string, varEdgeProp> map_nameToProps);
    void printAllEdges();

    QSeqEditor* parentEditor;


};

#endif // QCPSEQUENCEDATA_H
