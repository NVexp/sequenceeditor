#include "qseqeditor.h"
#include <iostream>

QSeqEditor::QSeqEditor(QWidget* parentWidget): QCustomPlot(parentWidget)
    , step(10.)
    , Tmax (511.*10.)
{
    seqData = new QCPSequenceData(xAxis, yAxis, this);

    //initialize to 0 everywhere
    long N = lround(Tmax/step);
    QVector<double> t;
    QVector<double> l (N+1, 0.);

    for (int i=0; i<N+1; ++i)
    {
        t.push_back(i*step);
    }
    seqData->setData(t, l, true);

    xAxis->setRange(0, Tmax/4.);
    yAxis->setRange(-.4,1.1);
    //setInteraction(QCP::iSelectPlottables);
    axisRect()->setRangeDrag(Qt::Horizontal);
    axisRect()->setRangeZoom(Qt::Horizontal);
    setInteraction(QCP::iRangeDrag);
    setInteraction(QCP::iRangeZoom),
    seqData->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, 5));
    seqData->setSelectable(QCP::stSingleData);
    xAxis->grid()->setVisible(false);
    yAxis->grid()->setVisible(false);

    std::cout << "QSeqEditor constructed" << std::endl;
}

