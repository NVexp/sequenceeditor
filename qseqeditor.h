#ifndef QSEQEDITOR_H
#define QSEQEDITOR_H
#include "qcustomplot.h"
#include "qcpsequencedata.h"


class QSeqEditor : public QCustomPlot
{
    Q_OBJECT
public:
    QSeqEditor(QWidget* parentWidget);
    QCPSequenceData* seqData;
    double step, Tmax;

};

#endif // QSEQEDITOR_H
