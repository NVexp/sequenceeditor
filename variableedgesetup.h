#ifndef VARIABLEEDGESETUP_H
#define VARIABLEEDGESETUP_H

#include <QDialog>

namespace Ui {
class variableEdgeSetup;
}

class variableEdgeSetup : public QDialog
{
    Q_OBJECT

public:
    explicit variableEdgeSetup(QWidget *parent = nullptr);
    ~variableEdgeSetup();

private slots:
    void on_buttonBox_accepted();

private:
    Ui::variableEdgeSetup* ui;
};

#endif // VARIABLEEDGESETUP_H
